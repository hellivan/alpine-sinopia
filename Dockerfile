FROM mhart/alpine-node:6.2.2

EXPOSE 4873
VOLUME /srv/sinopia
VOLUME /etc/sinopia

RUN npm install -g sinopia@1.4.0 &&\
    npm cache clean

ADD ./config.yaml /etc/sinopia/config.yaml

CMD ["sinopia", "-c", "/etc/sinopia/config.yaml"]